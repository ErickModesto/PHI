# -*- coding: utf-8 -*-

import RPi.GPIO as io   #load gpio library
import Adafruit_CharLCD as LCD #load display lcd library
import Adafruit_DHT    #load dht sensor library
import time
import socket
import sys
import threading 
try:
	io.cleanup(); #Unexport pins
	io.setwarnings(False) #do not show any warning
	io.setmode(io.BCM)
	io.setup(19, io.OUT) #set GPIO19 as output
	
	#io.setup(20, io.IN, pull_up_down = io.PUD_DOWN)
	#io.setup(16, io.IN, pull_up_down = io.PUD_DOWN)
	
	p = io.PWM(19, 50)   #set GPIO19 pwm with 50hz frequency
	   
	#7.5  ------  90 graus
	#2.5  ------   0 graus
	#12.5 ------ 180 graus   
	
	p.start(7.5)
	
	sensor = Adafruit_DHT.DHT11 
	
	pin_sensor = 21
	
	lcd_rs        = 27
	lcd_en        = 22
	lcd_d4        = 25
	lcd_d5        = 24
	lcd_d6        = 23
	lcd_d7        = 18
	lcd_backlight = 4
	
	lcd_colunas = 16
	lcd_linhas  = 2
	
	#io.setup(lcd_en, io.OUT)
	#io.setup(lcd_rs, io.OUT)
	#io.setup(lcd_d4, io.OUT)
	#io.setup(lcd_d5, io.OUT)
	#io.setup(lcd_d6, io.OUT)
	#io.setup(lcd_d7, io.OUT)
	#io.setup(lcd_backlight, io.OUT)
	
	lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5, lcd_d6, lcd_d7, lcd_colunas, lcd_linhas, lcd_backlight)
	lcd.clear()
	#io.output(lcd_backlight, True)
	
	
	lcd.message("LCD ok")
	
	HOST = '' # Server IP or Hostname deixa desse jeito mesmo seu bosta de merda
	PORT = 50000 # Pick an open Port (1000+ recommended), must match the client
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	
	try:
		s.bind((HOST, PORT))
	except socket.error:
		print "Bind failed"

	s.listen(5)
	while 1:
		
		umidade, temperatura = Adafruit_DHT.read_retry(sensor, pin_sensor)
		print "Socket awaiting messages"
		(conn, addr) = s.accept()
		print "Connected"
		data = conn.recv(1024) 
		print "Mesagem recebida do cliente " + data  
		umidade, temperatura = Adafruit_DHT.read_retry(sensor, pin_sensor)
		
		if umidade < 85 or data == "ativar":
			lcd.clear()
			print ("\nTemperatura = {0:0.1f}  Umidade = {1:0.1f}%\n''").format(temperatura, umidade)
			lcd.message("Temp: {0:0.1f} graus C\nUmidade: {1:0.1f}%".format(temperatura, umidade))
			p.ChangeDutyCycle(12.5)
			conn.send("Sistema ativado!")
			time.sleep(2)
			
		elif umidade >= 85 or data == 'desativar':
			lcd.clear()
			print("Sistema Desativado!")
			lcd.message("Desativado")
			p.ChangeDutyCycle(7.5)
			conn.send("Sistema desativado!")
			ativado = False
			time.sleep(2)
			
		elif data == 'status':
			
			if ativado == True:
				lcd.clear()
				print("\nStatus do sistema: Temperatura = {0:0.1f}  Umidade = {1:0.1f}%''").format(temperatura, umidade)
				lcd.message("Temp: {0:0.1f} grausC\nUmidade: {1:0.1f}%".format(temperatura, umidade))
				conn.send("Sistema Ativado!\n Temp: {0:0.1f} grausC\nUmidade: {1:0.1f}%".format(temperatura, umidade))
			else:
				lcd.clear()
				print("\nStatus do sistema: Temperatura = {0:0.1f}  Umidade = {1:0.1f}%''").format(temperatura, umidade)
				lcd.message("Desativado")
				conn.send("Sistema desativado!\n Temp: {0:0.1f} grausC\nUmidade: {1:0.1f}%".format(temperatura, umidade))
				
				

except KeyboardInterrupt:
	conn.close()
	lcd.clear()
	io.cleanup()
	print "\nBye"
	sys.exit()
