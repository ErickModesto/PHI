# -*- coding: utf-8 -*-

import RPi.GPIO as io   #load gpio library
import Adafruit_CharLCD as LCD #load display lcd library
import Adafruit_DHT    #load dht sensor library
import time

io.cleanup(); #Unexport pins
io.setwarnings(False) #do not show any warning
io.setmode(io.BCM)
io.setup(19, io.OUT) #set GPIO19 as output

p = io.PWM(19, 50)   #set GPIO19 pwm with 50hz frequency
       
#7.5  ------  90 graus
#2.5  ------   0 graus
#12.5 ------ 180 graus   

p.start(7.5)

sensor = Adafruit_DHT.DHT11 

pin_sensor = 21

lcd_rs        = 27
lcd_en        = 22
lcd_d4        = 25
lcd_d5        = 24
lcd_d6        = 23
lcd_d7        = 18
lcd_backlight = 4

lcd_colunas = 16
lcd_linhas  = 2

#io.setup(lcd_en, io.OUT)
#io.setup(lcd_rs, io.OUT)
#io.setup(lcd_d4, io.OUT)
#io.setup(lcd_d5, io.OUT)
#io.setup(lcd_d6, io.OUT)
#io.setup(lcd_d7, io.OUT)
#io.setup(lcd_backlight, io.OUT)

lcd = LCD.Adafruit_CharLCD(lcd_rs, lcd_en, lcd_d4, lcd_d5,
                           lcd_d6, lcd_d7, lcd_colunas, lcd_linhas,
                           lcd_backlight)
lcd.clear()
#io.output(lcd_backlight, True)


lcd.message("LCD ok")
print ("\nLendo o sensor de umidade...")


while 1:
	
	umidade, temperatura = Adafruit_DHT.read_retry(sensor, pin_sensor)
	
	if umidade is not None and temperatura is not None:
		lcd.clear()
		print ("\nTemperatura = {0:0.1f}  Umidade = {1:0.1f}n\n''").format(temperatura, umidade)
		print ("\nAguarda 5 segundos para efetuar nova leitura...n")
		lcd.message("Temp: {0:0.1f} grausC\nUmidade: {1:0.1f}%".format(temperatura,
umidade))
		time.sleep(2)
	else:
		lcd.clear()
		print("\nNao")
		lcd.message("Nao")
		time.sleep(2)