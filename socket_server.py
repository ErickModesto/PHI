import socket
#from socket import *
import sys
HOST = '' # Server IP or Hostname
PORT = 50000 # Pick an open Port (1000+ recommended), must match the client
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
print "Socket created"
#managing error exception
try:
	s.bind((HOST, PORT))
except socket.error:
	print "Bind failed"
s.listen(5)
print "Socket awaiting messages"
(conn, addr) = s.accept()
print "Connected"

# awaiting for message
while True:
	data = conn.recv(1024)
	print "I sent a message back in response to: " + data
	
	# process your message
	if data == "ativar" or data == "Ativar":
		reply = "Sistema ativado!"
		conn.send(reply)
	elif data == "desativar" or data == "Desativar":
		reply = "Sistema desativado!"
		conn.send(reply)
	elif data == "quit":
		conn.send("Terminating")
		break
	(conn, addr) = s.accept()
	# Sending reply
#conn.send(reply)
conn.close() # Close connections
